import datetime
import json
import pandas as pd
import numpy as np
import dateutil.parser as dtread

import repo  # Has functions: extract_date, extract_time_from_object, get_all_fav_meals_pd,

                            # get_all_orders_pd, nextid, get_all_uids, getrows, s_fmeal, s_order, upload



######################
# Hardcoded values
threshold = 0.05
mincount = 2
enoughcount = 3
#####################

def get_contents(order_or_meal):
    return order_or_meal['contents']

def important_enough(matching_orders, user_orders):
    '''
    "Is this order significant enough, relative to the user's whole order history, to be recommended?"
    This is the current algorithm.
    
    `matching` is a list of user's orders that match some particular order (list of dicts)
    `user_orders` is the user's order history (list of dicts)
    
    Right now, this file calculates the avg time the meal was ordered.
    Once a meal is in the recommendations databse, the server recommends it to the user if 
        they are at the correct location within 1 standard deviation of that avg time.
    In theory the ML algorithm would group together orders that happen at similar times.
    '''
    count = len(matching_orders)
    weight = round(count / len(user_orders), 4)
    return (weight > threshold and count >= mincount) or (count >= enoughcount)


def orders_like_this_order(order, orders):
    '''
    `order` is a user's order (dict)
    `orders` is the user's order history (list of dicts)
    
    Return a list of all orders that match the order
    Each matching order is a dict
    '''
    matches = filter(
        (lambda order_x: get_contents(order_x)==get_contents(order) and order_x['loc_id']==order['loc_id']),
        orders
    )
    return list(matches)


def fav_meals_like_this_order(order, fav_meals):
    '''
    `order` is a user's order (dict)
    `fav_meals` is all the user's recommended meals (list of dicts)
    
    Return a list of all recommended meals that match the order
    List should have either 0 or 1 meals, or possibly more if 
        multiple existing favs overlap with `order`
    Each "meal" is a dict
    '''
    matches = filter(
            (lambda meal: get_contents(meal)==get_contents(order) and meal['primary_loc']==order['loc_id']),
            fav_meals
    )
    return list(matches)

def get_avg_time(matching_orders):
    '''
    Given a list of matching orders (dict),
    What is the average order time? What is the standard deviation?
    Return a datetime.time object and a datetime.timedelta object
    '''
    timestrings = []
    for match in matching_orders:
        timestrings.append(match['timestamp'])

    timelist_tobj = [repo.extract_date(string).time() for string in timestrings]
    # print('tobjs: ' + str(timelist_tobj))
    timelist = [t.hour+t.minute/60 for t in timelist_tobj]
    avg = np.average(timelist) * 60 # average in minutes since midnight
    # print('std in hours:' + str(std))
    avghour = int(avg//60)
    avgmin = int(avg%60)
    avgtime = datetime.time(hour=avghour, minute=avgmin)
    stdhours = np.std(timelist)
    if len(matching_orders)==1:
        std_td = datetime.timedelta(days=0.3)
    else:
        std_td = datetime.timedelta(seconds=3600*stdhours)
        # print('std obj: '+ str(std_td))
        std_td = max(std_td, datetime.timedelta(seconds=60))
        std_td = min(std_td, datetime.timedelta(days=0.3))
    return avgtime, std_td
    


    # If user has multiple recommendations, server sends the one with highest `weight`
    # mid == meal_id
def make_fav_meal_raw(matching_orders, mid, weight=None):
    '''
    order is a dict. orders is a list of dicts
    helper function to generate a 'meal recommendation' table entry based on order `order`
    '''
    avg_time, std_time = get_avg_time(matching_orders)
    order = matching_orders[0]
    meal = {'id': mid,
            'uid': order['uid'],
            'contents': json.dumps(order['contents']),
            'time': str(avg_time),
            'stdev_time': str(std_time),
            'primary_loc': order['loc_id'],
            'weight': weight
        }
    return meal



def verify(doing_what, printif=None, printelse=None):
    # For i/o purposes
    print('Do you want to ' + doing_what + '?')
    truth = input("'y' if yes; ENTER otherwise: ")=='y'
    if printif is not None and truth:
        print(printif)
    if printelse is not None and not truth:
        print(printelse)
    return truth


def update_fav_meals(quiet, uids, removal, current_fmeals, orders, next_fmeal_id=1):
    """
    Where "fav meal" (or "fmeal") refers to a meal that we are recommending
    - determine whether any recommended meals should be created or modified,
    - and make necessary modifications.
    """
    new_favs = []
    changed_faves = set()
    ignore = set()
    ignore_orders = []
    manual_collisions = []
    removed_faves = set()
    for uid in uids:     # For each existing user
        user_current_fmeals = repo.getrows(current_fmeals, 'uid==%s'%uid) # Query the user's current (pre-existing) recommendations
        # Format table data for python operations
        try:
            user_current_fmeals = [repo.s_fmeal(m) for m in user_current_fmeals]
        except:
            if not quiet:
                print('error with uid', uid)
            return
        user_orders = repo.getrows(orders, 'uid==%s'%uid)   # Query the user's order history
        user_orders = [repo.s_order(o) for o in user_orders]
        # For each of the users orders, check if any matching recommendations already exist (and assign list to `current_res`)
        for order in user_orders: # each order is a dict
            if order not in ignore_orders:
                matching_orders = orders_like_this_order(order, user_orders)
                ignore_orders.extend(matching_orders)
                current_res = fav_meals_like_this_order(order, user_current_fmeals)
                new_weight = round(len(matching_orders)/len(user_orders), 4)
                if len(current_res)>1:
                    if quiet:
                        manual_collisions.append(current_res)
                    else:
                        print('There are multiple matching fmeals with ids', current_res)
                        print('We will work with fmeal', current_res[0]['id'])
                        print('Please resolve the collision manually.')
                if len(current_res)==0: # Order has no corresponding recommended meal
                    # Decide whether the order should be listed
                    if important_enough(matching_orders, user_orders):
                        new_raw_meal = make_fav_meal_raw(matching_orders, next_fmeal_id, new_weight) # a dict
                        next_fmeal_id += 1
                        ignore.add(next_fmeal_id)
                        series = pd.Series(new_raw_meal, name=current_fmeals.shape[0])
                        new_favs.append(new_raw_meal) # ""
                        current_fmeals = current_fmeals.append(series) # ADD NEW ONES
                    else: # Order should not be listed!
                        # You could check to see if any favs correspond to this order, and delete favs
                        # that shouldn't be there.
                        # However, some of the favs are hardcoded by me, and I don't want to delete them.
                        # So I won't implement this part.
                        pass
                else: # Order matches a recommended meal
                    fav = current_res[0]
                    if (removal) and (fav['id']!=20) and (not important_enough(matching_orders, user_orders)):
                        # print('Weight is '+ str(new_weight) + '; count is '+str(len(matching_orders)))
                        current_fmeals = current_fmeals[current_fmeals.id!=fav['id']] # REMOVE OLD ONES
                        # current_fmeals.index = range(current_fmeals.shape[0]) # reindex
                        current_fmeals = current_fmeals.reindex()
                        removed_faves.add(fav['id'])
                    if fav['id'] not in changed_faves.union(ignore).union(removed_faves):
                        # recalculate the time at which it should be recommended
                        # This should probably also update the 'weight' (relative significance)
                        #       of the meal, but does not yet do so.
                        new_time, stdev_time = get_avg_time(matching_orders)
                        if str(new_time)==fav['time'] and str(stdev_time)==fav.get('stdev_time') and new_weight==round(fav.get('weight'), 4): 
                            # no new information
                            ignore.add(fav['id'])
                        else:
                            # update fmeal table
                            # print(stdev_time)
                            # print("Time was "+ fav['time'] + ", will be " + str(new_time))
                            # print("Timedelta was "+ fav['stdev_time'] + ", will be " + str(stdev_time))
                            # print("weight was "+ str(fav['weight']) + ", will be " + str(new_weight))
                            current_fmeals.loc[current_fmeals.id==fav['id'], 'time'] = str(new_time)        # MODIFY EXISTING ONES
                            current_fmeals.loc[current_fmeals.id==fav['id'], 'stdev_time'] = str(stdev_time) # ""
                            current_fmeals.loc[current_fmeals.id==fav['id'], 'weight'] = new_weight         # ""
                            changed_faves.add(fav['id']) # "" 
    return new_favs, changed_faves, current_fmeals, manual_collisions, removed_faves


def download_update_upload_fav_meals(quiet=False, uid=None, removal=False):
    """
    Where "fav meal" (or "fmeal") refers to a meal that we are recommending
    
    - Download current order history and recommended meals (once),
    - determine whether any recommended meals should be created or modified,
    - make necessary modifications,
    - and then upload new recommendation table (once).
    """
    current_fmeals = repo.get_all_fav_meals_pd() # Download data for existing recommendations
    orders = repo.get_all_orders_pd()            # Download data for order history
    next_fmeal_id = repo.nextid(current_fmeals)
    if uid is None:
        uids = repo.get_all_uids()
    else:
        uids = uid[0]

    new_favs, changed_faves, current_fmeals, manual_collisions, removed_faves = update_fav_meals(quiet, uids, removal, current_fmeals, orders, next_fmeal_id)

    if quiet:
        return deal_with_info_quietly(new_favs, changed_faves, current_fmeals, manual_collisions)
    if removal:
        print('Recommendations to be removed: '+str(removed_faves))
    return deal_with_info_loudly(new_favs, changed_faves, current_fmeals, len(removed_faves)>0)

def _what_do_I_need():
    return {
        'contents': '(dict; optionally jsonified)',
        'id': '(int, unique, optional)',
        'loc_id': '(int)',
        'price': '(float, optional)',
        'timestamp' '(str, formatted like a datetime.datetime object\'s toString - eg "2019-02-28 07:55:00-05:00")'
        'uid': '(int)'
    }

def _testing__generate_some_fav_meals(orders, uids=None):
    """
    orders is a list of dicts
        each has attributes:
            'contents' (dict; optionally jsonified),
            'id' (int, unique, optional),
            'loc_id' (int),
            'price' (float, optional),
            'timestamp' (str, formatted like a datetime.datetime object's toString - eg "2019-02-28 07:55:00-05:00")
            'uid' (int)
    """
    for i in range(len(orders)):
        o = orders[i]
        if isinstance(o['contents'], dict):
            o['contents'] = json.dumps(o['contents'])
        if 'id' not in o.keys():
            o['id'] = i + 1
        if 'price' not in o.keys():
            o['price'] = 1.00
    quiet = False
    if uids is None:
        uids = list(set([o['uid'] for o in orders]))
    removal=True
    current_fmeals = pd.DataFrame()
    current_fmeals.index.rename('key')
    x = current_fmeals
    x['contents'], x['id'], x['primary_loc'], x['time'], x['uid'], x['weight'], x['stdev_time'] = '', 0, 0, '', 0, 0, ''
    order_df = pd.DataFrame(orders)
    print('uids:', uids)
    a, b, c, d, e = update_fav_meals(quiet, uids, removal, current_fmeals, order_df)
    return {
        'new_favs': a,
        'changed_faves': b,
        'current_fmeals': c,
        'manual_collisions': d,
        'removed_faves': e
    }

    # (quiet, uids, removal, current_fmeals, orders, next_fmeal_id=1, allnew_test=False):


def deal_with_info_loudly(new_favs, changed_faves, current_fmeals, remTrue):
    if len(new_favs)==0:
        print('No brand-new fmeals (suggested meals)')
    else:
        verify('print brand-new fmeals (suggested meals)', 'new favs:\n'+str(new_favs))
    if len(changed_faves)==0:
        print('No changed fmeals')
    else:
        verify('print changed fmeals', 'Time or stdev_time or weight was changed for fmeals with these ids:\n'+str(changed_faves))
    if len(new_favs) + len(changed_faves) + remTrue == 0 :
        print('Nothing to upload.')
    else:
        if verify('upload changes', 'Uploading', 'Not uploading'):
            success = repo.upload('fmeal', current_fmeals) # Upload data
            print('Success:', success)

def deal_with_info_quietly(new_favs, changed_faves, current_fmeals, manual_collisions):
    if len(new_favs) + len(changed_faves) > 0:
        success = repo.upload('fmeal', current_fmeals) # Upload data
        data = [fav['id'] for fav in new_favs], list(changed_faves), manual_collisions
        return success, data
    else:
        return True, None


if __name__=='__main__':
    from sys import argv
    rem = 'removal' in argv
    download_update_upload_fav_meals(removal=rem)


